package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.List;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    List<Spell> spellList;

    public ChainSpell(List<Spell> spellList){
        this.spellList = spellList;
    }

    @Override
    public void cast() {
        for (Spell spell : spellList){
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for (Spell spell : spellList){
            spell.undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
