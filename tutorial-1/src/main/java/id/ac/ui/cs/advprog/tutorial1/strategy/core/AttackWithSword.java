package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    //ToDo: Complete me

    public AttackWithSword(){}

    public String attack(){
        return "Attacking with Sword! Slashh";
    }

    public String getType(){
        return "Sword";
    }
}
