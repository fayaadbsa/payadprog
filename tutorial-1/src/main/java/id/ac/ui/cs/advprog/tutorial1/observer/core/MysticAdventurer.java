package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me
        this.guild = guild;
    }

    //ToDo: Complete Me
    public void update() {
        String questType = guild.getQuestType();
        if (questType.equals("D") || questType.equals("E")){
            this.getQuests().add(guild.getQuest());
        }
    }
}
