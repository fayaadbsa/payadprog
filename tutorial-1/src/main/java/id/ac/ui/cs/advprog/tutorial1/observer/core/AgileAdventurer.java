package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        //ToDo: Complete Me
        this.guild = guild;
    }

    //ToDo: Complete Me
    public void update() {
        String questType = guild.getQuestType();
        if (questType.equals("D") || questType.equals("R")){
            this.getQuests().add(guild.getQuest());
        }
    }
}
